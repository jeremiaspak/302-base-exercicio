class { "java":
    package => "openjdk-8-jdk",
}

include nginx

nginx::resource::server { "meuapp":
    listen_port => 80,
    proxy => "http://localhost:8080",
}

file { "/etc/nginx/conf.d/default.conf":
    ensure => absent,
    notify => Service["nginx"],
}

ssh_authorized_key { "jenkins@banana":
    ensure => present,
    user => "ubuntu",
    type => "ssh-rsa",
    key => "aaa",
}
